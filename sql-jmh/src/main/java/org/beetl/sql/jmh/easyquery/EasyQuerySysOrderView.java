package org.beetl.sql.jmh.easyquery;

import lombok.Data;

/**
 * create time 2023/8/3 09:20
 * 文件说明
 *
 * @author xuejiaming
 */
@Data
public class EasyQuerySysOrderView {
	private Integer id;
	private String name;
	private Integer customerId;
}

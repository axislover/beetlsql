#3.23.1

* 批量操作，可选逐条打印

#3.23.3

* Oracle 同位词数据获取

# 3.23.4

* Oracle 12翻页新的实现

# 3.23.9
* 增加sql-ext,包含了各种扩展，有xml支持，sql防火墙(firewall),性能优化

# 3.24.0
* 非javabean的pojo支持
* 加速扩展包重构
* 安全扩展包

# 3.25.0
* 扩展包sql重写，能支持多租户，数据权限等功能
* 
# 3.25.1
* DebugInterceptor 修复
* 
# 3.25.3
* BaseMapper增加batchUpdate实现 修复
* 优化SqlRewrite的RewriteMapper


